#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from datetime import datetime
from enum import auto, Enum
import git
from .logging import debug
import re
from typing import Tuple
from .exceptions import NothingToUprev


class UpdateRevision(Enum):
    mesa_in_virglrenderer = auto()
    piglit_in_mesa = auto()


class UprevActionProcessor:
    __actions: list = None
    __index: git.index.base.IndexFile = None
    __old_revision: str = None
    __new_revision: str = None

    def __init__(self, actions: list, index: git.index.base.IndexFile):
        self.__actions = actions
        self.__index = index

    @property
    def old_revision(self):
        return self.__old_revision

    @property
    def new_revision(self):
        return self.__new_revision

    def process_actions(self) -> None:
        """
        With a structure describing the replacements to be made by regular
        expressions to the files involved in the uprev, apply the strategy.
        :param actions: list with dictionaries of replacements in files
        :param index: to add the modified file to be included in the commit
        :return:
        """
        for action in self.__actions:
            file_name: str = action["file_name"]
            missing_replacements: set[tuple[str, str]] = set(
                action.get("replacements", ())
            )
            missing_requirements: set[str] = set(action.get("require", ()))
            is_uprev_action = action.get("uprev", False)
            with open(file_name, "rt") as file_descriptor:
                contents: list[str] = file_descriptor.readlines()
            for i, line in enumerate(contents):
                if missing_requirements := self.__update_requirements(
                    missing_requirements, i, line
                ):
                    # nothing to replace until the list is empty
                    continue
                missing_replacements = self.__update_replacements(
                    missing_replacements, contents, i, line, is_uprev_action
                )
                if not missing_replacements:
                    # When this list is empty, no necessary to read the next
                    break
            if missing_replacements or missing_requirements:
                if missing_replacements:
                    debug(
                        f"Unexpected situation: parsed the file and there "
                        f"are still missing replacements:\n"
                        f"{missing_replacements}"
                    )
                if missing_requirements:
                    debug(
                        f"Unexpected situation: parsed the file and there "
                        f"are still missing requirements:\n"
                        f"{missing_requirements}"
                    )
                raise SyntaxError("Pending actions when End of the File reached")
            with open(file_name, "wt") as file_descriptor:
                file_descriptor.writelines(contents)
            self.__index.add(file_name)

    @staticmethod
    def __update_requirements(
        missing_requirements: set[str], i: int, line: str
    ) -> set[str]:
        """
        This method is made to manage the requirements an action could have to
        satisfy before look for the patterns to replace.
        :param missing_requirements: a set of conditions
        :param line: the current line of the file
        :return: list of not yet satisfied requirements.
        """
        found_requirements: set[str] = set()
        if missing_requirements:
            for requirement in missing_requirements:
                if re.search(requirement, line):
                    # as the pattern has been found, remove from the set
                    found_requirements.add(requirement)
                    continue
            missing_requirements.difference_update(found_requirements)
        return missing_requirements

    def __update_replacements(
        self,
        missing_replacements: set[tuple[str, str]],
        contents: list[str],
        i: int,
        line: str,
        is_uprev_action: bool,
    ) -> set[tuple[str, str]]:
        """
        This method is made to manage the replacements an uprev action must do to
        continue with the procedure.
        :param missing_replacements: pending replacements for an action
        :param contents: list with all the lines in the file
        :param i: current line under processing
        :param line: content of the line under processing
        :param is_uprev_action: identify the core action
        :return: refresh list of replacement to be made
        """
        found_replacements: set[list[str, str]] = set()
        old_field, new_field = None, None
        for pattern, new_field in missing_replacements:
            if search_pattern := re.search(pattern, line):
                old_field = search_pattern[1]
                if is_uprev_action and old_field == new_field:
                    raise NothingToUprev
                contents[i] = line.replace(old_field, new_field)
                # store found pattens once they are used
                found_replacements.add((pattern, new_field))
        missing_replacements.difference_update(found_replacements)
        if is_uprev_action:
            self.__old_revision = old_field
            self.__new_revision = new_field
        return missing_replacements


def uprev_mesa_in_virglrenderer(
    repo: git.Repo, pipeline_id: int, revision: str, templates_commit: str
) -> str:
    """
    Apply the necessary changes to the local clone in the working branch to
    proceed with the uprev of mesa project in virglrenderer
    :param repo: object of the local clone
    :param pipeline_id: reference pipeline of the dep project to uprev
    :param revision: dep project revision to uprev
    :param templates_commit: ci-templates commit to uprev
    :return: Note for the merge request
    """
    index = repo.index  # This is expensive, reuse the index object
    actions = [
        {
            "file_name": ".gitlab-ci.yml",
            "replacements": (
                (
                    r".*MESA_TEMPLATES_COMMIT: &ci-templates-commit "
                    r"([0-9a-fA-F].*)$",
                    templates_commit,
                ),
                (r".*MESA_PIPELINE_ID:.* ([0-9].*)$", f"{pipeline_id}"),
            ),
        },
        {
            "file_name": ".gitlab-ci.yml",
            "require": (r".*- project:.*mesa/mesa.*",),
            "replacements": ((r".*ref: ([0-9a-fA-F].*)$", revision),),
            "uprev": True,
        },
    ]
    processor = UprevActionProcessor(actions, index)
    processor.process_actions()
    old_revision, new_revision = processor.old_revision, processor.new_revision
    diff_url = (
        f"https://gitlab.freedesktop.org/mesa/mesa/-/"
        f"compare/{old_revision}...{new_revision}"
    )
    index.commit(f"Uprev Mesa to {revision}\n\n{diff_url}")
    return diff_url


def uprev_piglit_in_mesa(
    repo: git.Repo,
    revision: str,
) -> str:
    """
    Apply the necessary changes to the local clone in the working branch to
    proceed with the uprev of piglit project in mesa
    :param repo: object of the local clone
    :param revision: dep project revision to uprev
    :return: Note for the merge request
    """
    index = repo.index  # This is expensive, reuse the index object
    today_date = datetime.strftime(datetime.now(), "%Y-%m-%d")
    image_tag = f"{today_date}-piglit-{revision[:8]}"
    actions = [
        {
            "file_name": ".gitlab-ci/container/build-piglit.sh",
            "replacements": ((r"REV=\"(\w.*)\"", revision),),
            "uprev": True,
        },
        {
            "file_name": ".gitlab-ci/image-tags.yml",
            "replacements": (
                (r"DEBIAN_X86_64_TEST_GL_TAG: \"(.*)\"", image_tag),
                (r"DEBIAN_X86_64_TEST_VK_TAG: \"(.*)\"", image_tag),
                (r"KERNEL_ROOTFS_TAG: \"(.*)\"", image_tag),
            ),
        },
    ]
    processor = UprevActionProcessor(actions, index)
    processor.process_actions()
    old_revision, new_revision = processor.old_revision, processor.new_revision
    diff_url = (
        f"https://gitlab.freedesktop.org/mesa/piglit/-/compare/"
        f"{old_revision}...{new_revision}"
    )
    index.commit(f"Uprev Piglit to {revision}\n\n{diff_url}")
    return diff_url
