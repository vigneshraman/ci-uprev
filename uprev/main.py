#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from argparse import ArgumentParser, Namespace
import base64
from collections import defaultdict, deque
from copy import deepcopy
from datetime import datetime
from functools import cache
import git
import gitlab
from gitlab.v4.objects import (
    Project,
    ProjectBranch,
    ProjectCommit,
    ProjectIssue,
    ProjectJob,
    ProjectMergeRequest,
    ProjectNote,
    ProjectPipeline,
    ProjectPipelineJob,
)
import json
import os
from pprint import pformat
import re
from shutil import rmtree
import sys
from tenacity import retry, stop_after_attempt, wait_exponential
import time
from typing import Optional, Tuple, Union

from .environment import (
    GitlabProxy,
    Target,
    Fork,
    Dependency,
    Working,
    Template,
    UpdateRevisionPair,
    LocalClone,
    in_production,
    Branch,
    Title,
)
from .exceptions import NothingToUprev
from .expectations import mesa_expectations_paths, virglrenderer_expectations_paths
from .logging import debug
from .uprevs import UpdateRevision, uprev_mesa_in_virglrenderer, uprev_piglit_in_mesa

# GLOBAL constants

LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE: int = 10
MAX_LINES_PER_CATEGORY_IN_NOTE: int = 100

CHECKER_SLEEP_TIME: int = 10  # seconds
PIPELINE_CREATION_TIMEOUT: int = 600  # seconds, 10 minutes


def defaultdictdeque() -> defaultdict:
    return defaultdict(deque)


def get_candidate_for_uprev() -> Tuple[int, str]:
    """
    First get pipelines in the project where it has to update the revision.
    Then, chose the first one that satisfy specific conditions.
    :return: pipeline id and commit hash
    """
    pipelines = __get_pipelines()
    pipeline = __select_pipeline(pipelines)
    if not pipeline:
        raise LookupError("No uprev candidate found")
    return pipeline.id, pipeline.sha


def __get_pipelines(
    status: str = "success", username: str = "marge-bot"
) -> gitlab.base.RESTObjectList:
    """
    To update the revision, the first requisite is a pipeline that:
    1. It has to have succeeded,
    2. It has to be merged by marge
    :return: iterator over pipelines
    """
    dependency_project = Dependency().gl_project
    return dependency_project.pipelines.list(
        status=status,
        username=username,
        ordered_by="created_at",
        sort="desc",
        iterator=True,
    )


def __select_pipeline(pipelines: gitlab.base.RESTObjectList) -> ProjectPipeline:
    """
    Specific condition that the pipeline candidate must satisty. In the case of
    mesa for virglrenderer it is necessary (because it uses some artifacts from
    the merge pipeline) to have the 'debian-testing' job.
    :return: the pipeline to use in the uprev.
    """
    uprev_pair = UpdateRevisionPair().enum
    for pipeline in pipelines:
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            debian_testing_job = [
                job
                for job in pipeline.jobs.list(iterator=True)
                if job.name == "debian-testing"
            ]
            if debian_testing_job:
                return pipeline
        else:
            return pipeline


def get_templates_commit(revision: str, filename: str = ".gitlab-ci.yml") -> str:
    """
    Find in a file of a gitlab project the definition of the
    MESA_TEMPLATES_COMMIT hash to be used.
    :param revision: define with commit, tag, branch use to read the file
    :param filename: file of the project where the variable is defined
    :return: hash of ci-templates to use in the uprev
    """
    template_project = Template().gl_project
    file = template_project.files.get(filename, ref=revision)
    file_content = base64.b64decode(file.content).decode("utf-8")
    for line in file_content.split("\n"):
        # sample line '    MESA_TEMPLATES_COMMIT: &ci-templates-commit <hash>'
        # and we are interested in the hash at the end
        pattern = r"MESA_TEMPLATES_COMMIT:.* ([0-9a-fA-F].*)$"
        if search_result := re.search(pattern, line):
            return search_result.group(1)


def create_branch(
    pipeline_id: int,
    revision: str,
    templates_commit: str,
) -> git.Repo:
    """
    With the uprev information, clone locally the target project and prepare
    the temporal working branch from the target's default branch. Then do the
    uprev itself and commit it.
    :param pipeline_id: reference pipeline of the dep project to uprev
    :param revision: dep project revision to uprev
    :param templates_commit: ci-templates commit to uprev
    :return: git repository object
    """
    repo = __clone_repo(force_fresh_repo=False)
    __clean_previous_tmp_branch()
    target_project = Target().gl_project

    repo.heads[target_project.default_branch].checkout(force=True, b=Branch().for_wip)
    debug(
        f"start working from the commit with SHA {repo.head.commit.hexsha} "
        f"{target_project.commits.get(repo.head.commit.hexsha).web_url}"
    )

    os.chdir(repo.working_dir)
    try:
        uprev_pair = UpdateRevisionPair().enum
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            dependency_diff = uprev_mesa_in_virglrenderer(
                repo, pipeline_id, revision, templates_commit
            )
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            dependency_diff = uprev_piglit_in_mesa(repo, revision)
        else:
            raise NotImplementedError(
                f"Not yet implemented the uprev of " f"{uprev_pair.name}"
            )
        Dependency().diff = dependency_diff
        debug(
            f"Created commit with SHA {repo.head.commit.hexsha}. Changes in "
            f"the dependency {dependency_diff}"
        )
    finally:
        os.chdir("..")

    return repo


def __clone_repo(
    force_fresh_repo: bool,
) -> git.repo:
    """
    Prepare a local clone of the repository with the target project and its
    fork. It can reuse or clean a previous clone. And in case of clone from
    scratch, setup the git config.
    :param force_fresh_repo: flag what to do if the local clone already exists
    :return:
    """
    local_clone = LocalClone().directory
    target_project = Target().gl_project
    if force_fresh_repo:
        if os.path.exists(local_clone):
            debug(f"Remove previously existing repo in {local_clone}")
            rmtree(local_clone)
    if os.path.exists(local_clone):
        debug(f"Reusing repo in {local_clone}")
        repo = git.Repo(local_clone)
        repo.remote("origin").update()
        repo.remote("fork").update()

        tmp_branch = Branch().for_wip
        if tmp_branch in repo.heads:
            debug(f"Deleting existing branch")
            repo.heads[target_project.default_branch].checkout(force=True)
            repo.delete_head(tmp_branch, force=True)
    else:
        username = GitlabProxy().gl_obj.user.username
        useremail = GitlabProxy().gl_obj.user.email
        token = GitlabProxy().token
        project_path = Target().path_with_namespace
        fork_path = Fork().path_with_namespace

        debug(f"Cloning repo at {local_clone}")
        repo_url = f"https://gitlab.freedesktop.org/{project_path}.git"
        repo = git.Repo.clone_from(
            repo_url, local_clone, branch=target_project.default_branch
        )
        repo.config_writer().set_value(
            "user", "name", "Collabora's Gfx CI Team"
        ).release()
        repo.config_writer().set_value("user", "email", useremail).release()
        fork_url = f"https://{username}:{token}@gitlab.freedesktop.org/{fork_path}.git"
        repo.create_remote("fork", url=fork_url)
    return repo


def __clean_previous_tmp_branch() -> None:
    """
    The temporary branch could still be in the fork repo because there is
    another uprev procedure in progress or because one failed lefting it orphan.
    So, when it is orphan a newer uprev procedure could fail and requires it
    to be cleaned.
    :return:
    """
    uprev_tmp_branch = Branch().for_wip
    if (branch := __get_branch(uprev_tmp_branch)) is None:
        return
    debug(f"The branch {uprev_tmp_branch} exists in the fork")
    __wait_for_running_pipelines_in_branch(branch)
    debug(f"Remove orphan {uprev_tmp_branch} branch")
    branch.delete()


def __get_branch(name: str) -> Optional[ProjectBranch]:
    """
    Get the branch gitlab object or return None hiding any exception
    :param name: string with branch name
    :return: The project branch object or None
    """
    try:
        fork_project = Fork().gl_project
        return fork_project.branches.get(name)
    except gitlab.exceptions.GitlabGetError:
        return


def __wait_for_running_pipelines_in_branch(branch: ProjectBranch) -> None:
    """
    When a branch have pipelines running, wait until they finish. They could be
    part of a procedure that could fail if we intervene.
    :param branch: branch that could have pipelines running.
    :return:
    """
    fork_project = Fork().gl_project
    uprev_tmp_branch_name = Branch().for_wip
    while active_pipelines := [
        pipeline.id
        for pipeline in fork_project.pipelines.list(
            sha=branch.commit["id"], iterator=True
        )
        if pipeline.status == "running"
    ]:
        if len(active_pipelines) > 0:
            debug(
                f"Branch {uprev_tmp_branch_name} has {len(active_pipelines)} "
                f"pipeline(s) running. Waiting them to finish... "
                f"({active_pipelines})"
            )
            time.sleep(CHECKER_SLEEP_TIME)


def push(repo: git.Repo) -> ProjectPipeline:
    """
    Push the commits in the local repository to the fork and wait until the
    last commit has a pipeline created.
    :param repo: local clone
    :return: commit pipeline
    """
    remote = repo.remote("fork")
    remote.push(force=True)
    Working().project = Fork()
    return __wait_pipeline_creation(repo.head.commit.hexsha)


def __wait_pipeline_creation(commit_hash: str, source: str = None) -> ProjectPipeline:
    """
    For a pushed commit to the fork, Peaceful wait for the pipeline creation.
    We can use the 'source' parameter to fine-tune the pipeline wanted.
    :param commit_hash: string
    :param source: string
    :return: pipeline
    """
    debug(
        f"Waiting for pipeline to be created for {commit_hash} "
        f"{__get_commit(commit_hash).web_url}"
    )
    t_0 = datetime.now()
    query = {"sha": commit_hash}
    if source:
        query["source"] = source
    while True:
        pipeline = __get_pipeline(query)
        if pipeline:
            return pipeline
        # when not found
        if (datetime.now() - t_0).seconds >= PIPELINE_CREATION_TIMEOUT:
            raise RuntimeWarning(
                f"Timeout reached while waiting for pipeline creation for "
                f"commit {commit_hash}"
            )
        time.sleep(CHECKER_SLEEP_TIME)


@retry(stop=stop_after_attempt(10), wait=wait_exponential(multiplier=1, max=180))
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_commit(commit_hash: str) -> ProjectCommit:
    """
    Get a commit object from the working project. If there is an exception, log
    it and reraise. It has also a decorator to use tenacity to retry.
    :param commit_hash: identifier string of the commit1
    :return: object in gitlab representing a commit.
    """
    try:
        working_project = Working().gl_project
        return working_project.commits.get(commit_hash)
    except Exception as exception:
        debug(
            f"Couldn't get the commit {commit_hash} "
            f"due to {type(exception)}: {exception}"
        )
        raise exception


@retry(stop=stop_after_attempt(10), wait=wait_exponential(multiplier=1, max=180))
def __get_pipeline(query: dict) -> Optional[ProjectPipeline]:
    """
    Get the first pipeline in a list generated by a query. When the list is
    empty, return None. But if there is any other exception, it uses tenacity's
    retry.
    :param query: dictionary as the query_parameters in GL api
    :return: the pipeline or None
    """
    try:
        project = Working().gl_project
        pipeline = next(project.pipelines.list(query_parameters=query, iterator=True))
        return project.pipelines.get(pipeline.id)
    except StopIteration:
        return None


def run(pipeline: ProjectPipeline) -> dict:
    """
    Trigger the manual jobs in the pipeline (waiting, if necessary, to the
    complete creation of the pipeline in the server) and wait until the pipeline
    finishes.
    Then collect the artifacts. Give it a second chance if any jobs still need
    to produce artifacts. And also, run a second time the ones that made
    failures artifacts.
    :param pipeline: git lab object
    :param project: when pipeline to run is in a different project than the fork
    :return: failures dict
    """
    debug(f"Waiting for pipeline to be ready {pipeline.web_url}")
    working_project = Working().gl_project
    while pipeline.status != "manual":
        time.sleep(CHECKER_SLEEP_TIME)
        pipeline = working_project.pipelines.get(pipeline.id)
    debug(f"Triggering jobs for pipeline {pipeline.web_url}")
    for job in pipeline.jobs.list(iterator=True):
        if __pipeline_trigger_job_condition(job):
            pjob = working_project.jobs.get(job.id, lazy=True)
            pjob.play()
    pipeline = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline)
    return __post_process_pipeline(pipeline)


def __pipeline_trigger_job_condition(job: ProjectPipelineJob) -> bool:
    """
    Determines whether a pipeline job should be triggered based on certain
    conditions.
    :param job: The pipeline job to evaluate.
    :return: Boolean indicating whether the job should be triggered.
    """
    if job.status == "manual":
        return True
    return False


def __post_process_pipeline(pipeline: ProjectPipeline) -> dict:
    """
    Check if there are results in the pipeline jobs. When some job has finished
    without artifacts, give it a second chance to make them.
    To detect flakes in the tests, the failed jobs are retried to see if they
    produce the same outcome.
    :param pipeline: pipeline to post process
    :return: dictionary with the failures found
    """
    failures, artifacts, jobs_without_artifacts = collate_results(pipeline)
    if jobs_without_artifacts:
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, jobs_without_artifacts
        )
    if artifacts:
        _, failures, _ = __retry_flake_candidates(pipeline, failures, artifacts)

    return failures


def __wait_pipeline_start(pipeline: ProjectPipeline) -> ProjectPipeline:
    """
    Once a manual jobs are triggered in a pipeline, there is a (usually short
    but non-negligible) time
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    debug(
        f"Wait for pipeline {pipeline.web_url} to start " f"(status={pipeline.status})"
    )
    while True:
        status_changed, pipeline = __refresh_pipeline_status(pipeline)
        if status_changed and pipeline.status in ["running", "failed"]:
            debug(f"Pipeline {pipeline.web_url} in {pipeline.status} status")
            return pipeline
        time.sleep(CHECKER_SLEEP_TIME)


def __wait_pipeline_finished(pipeline: ProjectPipeline) -> ProjectPipeline:
    """
    Periodically check if the pipeline has finished.
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    if pipeline.status in ["failed"]:
        return pipeline
    debug(
        f"Wait for pipeline {pipeline.web_url} to finish " f"(status={pipeline.status})"
    )
    while True:
        status_changed, pipeline = __refresh_pipeline_status(pipeline)
        if status_changed and pipeline.status not in [
            "created",
            "waiting_for_resource",
            "preparing",
            "pending",
            "running",
        ]:
            debug(
                f"Pipeline {pipeline.web_url} finished " f"(status={pipeline.status})"
            )
            return pipeline
        time.sleep(CHECKER_SLEEP_TIME)


def __refresh_pipeline_status(
    pipeline: ProjectPipeline,
) -> Tuple[bool, ProjectPipeline]:
    """
    Check if the pipeline status has changed since the last object refresh.
    :param pipeline: ProjectPipeline
    :return: bool, ProjectPipeline
    """
    working_project = Working().gl_project
    previous_pipeline_status = pipeline.status
    pipeline = working_project.pipelines.get(pipeline.id)
    if previous_pipeline_status != pipeline.status:
        debug(
            f"Pipeline {pipeline.id} status change: "
            f"{previous_pipeline_status} -> {pipeline.status}"
        )
        return True, pipeline
    return False, pipeline


def __retry_jobs_without_artifacts(
    pipeline: ProjectPipeline, artifacts: dict, jobs_without_artifacts: list
) -> Tuple[ProjectPipeline, dict]:
    """
    if there are jobs without artifacts, lets give them another try
    :return:
    """
    debug(f"Some jobs didn't produce artifacts")
    working_project = Working().gl_project
    for job in jobs_without_artifacts:
        debug(f"Retry '{job.name}' for artifacts build.")
        pjob = working_project.jobs.get(job.id, lazy=True)
        pjob.retry()
    pipeline = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline)
    # collect results again, but:
    # - remembering artefacts already downloaded to avoid overwriting
    _, artifacts, jobs_without_artifacts = collate_results(
        pipeline, artifacts=artifacts
    )
    if len(jobs_without_artifacts) != 0:
        job_names = [job.name for job in jobs_without_artifacts]
        __uprev_failed_issue(
            pipeline,
            f"These jobs failed to produce artifacts for "
            f"a second time: {', '.join(job_names)}",
        )
        sys.exit(0)
    return pipeline, artifacts


def __retry_flake_candidates(
    pipeline: ProjectPipeline, failures: dict, artifacts: dict
) -> Tuple[ProjectPipeline, dict, dict]:
    """
    Do one retry in the jobs that may have failed to
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    working_project = Working().gl_project
    for job in pipeline.jobs.list(iterator=True):
        if job.status == "failed":
            debug(f"Retry '{job.name}' to discard flakes")
            pjob = working_project.jobs.get(job.id, lazy=True)
            pjob.retry()
    pipeline = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline)
    # collect results again, but:
    # - remembering failures to collect together new results
    # - remembering artefacts already downloaded to avoid duplication
    _, _, jobs_without_artifacts = collate_results(
        pipeline, failures=failures, artifacts=artifacts
    )
    if len(jobs_without_artifacts) != 0:
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, jobs_without_artifacts
        )
    failures, artifacts, _ = collate_results(
        pipeline, failures=failures, artifacts=artifacts, recollect=True
    )
    return pipeline, failures, artifacts


def __uprev_failed_issue(
    pipeline: ProjectPipeline,
    reason: str,
) -> None:
    """
    When the uprev produces jobs that doesn't produce artifacts, it means there
    is something deeper about the uprev that cannot be solved with an
    expectations update. Further information has to be provided to the
    developers to know about the situation.
    """
    job_id = os.getenv("CI_JOB_ID")
    if job_id is not None:
        gl = GitlabProxy().gl_obj
        uprev_project = gl.projects.get(os.getenv("CI_PROJECT_ID"))
        uprev_job = uprev_project.jobs.get(job_id)
        description = [
            f"This information comes from the execution of ci-uprev in job "
            f"[{uprev_job.id}]({uprev_job.web_url}).\n\n"
        ]
    else:
        description = [
            f"This information comes from a local execution of ci-uprev.\n\n"
        ]
    description.append(f"Related pipeline: " f"[{pipeline.id}]({pipeline.web_url}).\n")
    description.append(f"ci-uprev reported:\n\n\t{reason}\n")
    working_project = Working().gl_project
    uprev_commit = working_project.commits.get(pipeline.sha)
    start_commt = working_project.commits.get(uprev_commit.parent_ids[0])
    dep_project = Dependency().gl_project
    dep_commit = dep_project.commits.get(uprev_commit.title.split()[-1])
    target_project = Target().gl_project
    description.append(
        f"This happened while doing an update revision of {dep_project.name} "
        f"[revision {dep_commit.short_id}]({dep_commit.web_url}) in "
        f"{target_project.name} [revision "
        f"{start_commt.short_id}]({start_commt.web_url}) with the uprev "
        f"[commit {uprev_commit.short_id}]({uprev_commit.web_url})."
    )
    __publish_uprev_issue("".join(description))


def __publish_uprev_issue(description: str) -> None:
    """
    When is necessary to publish some information in an issue in the target
    project, this method does that. But it only creates a new issue if there
    isn't one already opened. If there is already an issue, it appends a note.
    :param description: body text of the issue
    """
    issue = __get_uprev_issue()
    issue_title = Title().for_issue
    target_project = Target().gl_project
    if issue is None:
        labels = __get_uprev_labels()
        issue = target_project.issues.create(
            {"title": issue_title, "description": description, "labels": labels}
        )
        issue = target_project.issues.get(issue.id)
        debug(f"Created an issue with the runtime warning {issue.web_url}")
        note = None
    else:
        note = issue.notes.create({"body": description})
        note_web_url = f"{issue.web_url}#note_{note.id}"
        debug(
            f"Append a note to an already existing issue with the runtime "
            f"warning {note_web_url}"
        )
    __link_issue_with_merge_request(issue, note)


def __get_uprev_issue() -> Optional[ProjectIssue]:
    """
    Search in the target project if there is already an issue open about this
    uprev.
    :return: if exists, the issue, else None
    """
    target_project = Target().gl_project
    query = {
        "state": "opened",
        "search": Title().for_issue,
        "in": "title",
        "order_by": "created_at",
    }
    try:
        issue = next(target_project.issues.list(query_parameters=query, iterator=True))
        return target_project.issues.get(issue.iid)
    except StopIteration:
        return None


def __get_uprev_labels() -> list:
    """
    Simple method where each target project can set up the labels issues or
    merge requests should have tagged with.
    :return: list of tags
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return ["ci"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return ["CI"]
    else:
        return []


def __link_issue_with_merge_request(
    issue: ProjectIssue, note: ProjectNote = None
) -> None:
    mr_title = Title().for_merge_request
    merge_request = __get_uprev_merge_request(mr_title)
    if isinstance(merge_request, ProjectMergeRequest):
        description = ["It has been not possible to propose a newer uprev."]
        if not isinstance(note, ProjectNote):
            description.append(f"Further information on the issue " f"{issue.web_url}")
        else:
            description.append(
                f"Further information on the issue note " f"{note.web_url}"
            )
        merge_request.notes.create({"body": "".join(description)})
    else:
        debug(
            f"There isn't a merge request in progress to report the creation "
            f"of the issue"
        )


def collate_results(
    pipeline: ProjectPipeline,
    failures: dict = None,
    artifacts: dict = None,
    recollect: bool = False,
) -> Tuple[dict, dict, list]:
    """
    Find and collect the artifacts of the jobs. Classify them in a dictionary
    or, if there isn't, collect those jobs in a list.
    :param pipeline: ProjectPipeline
    :param failures: dict
    :param artifacts: dict
    :param recollect: bool
    :return: failures, artifacts, without_artifacts
    """
    debug(f"Looking for failed jobs in pipeline {pipeline.web_url}")
    if artifacts is None:
        artifacts = dict()
    if failures is None:
        # failures dict has as keys the tripplet (test_suite, backend, api)
        #  as value it has a nested dict where the key is the test_name and
        #  the values are deque of ens states of the tests in different reties.
        failures = defaultdict(defaultdictdeque)
    else:
        failures = deepcopy(failures)  # propagate will be decided by return
    without_artifacts = []

    working_project = Working().gl_project
    for job in pipeline.jobs.list(iterator=True):
        job = working_project.jobs.get(job.id)

        if job.status != "failed":
            continue

        if __is_fundamental_job(job):
            debug(
                f"Something very bad happened: "
                f"a build or sanity test job failed! "
                f"({job.name} job in {job.stage} stage)"
            )
            sys.exit(0)

        job_classification_key = __classify_the_job(job)

        if __download_artifacts(job, artifacts):
            results = artifacts[job.name]
        else:
            without_artifacts.append(job)
            continue

        __collate_failures(results, failures[job_classification_key], recollect)

    if recollect:
        __implicit_results_become_explicit(failures)

    return failures, artifacts, without_artifacts


def __is_fundamental_job(job: ProjectJob) -> bool:
    """
    Different uprev would have different definitions to classify their jobs
    between what's a fundamental job in the pipeline. They are jobs that create
    things that are required in other jobs.
    :param job: ProjectJob
    :return: bool
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return job.stage in ["build", "sanity test"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return job.stage in [
            "sanity",
            "container",
            "build-x86_64",
            "build-misc",
            "lint",
        ]


def __classify_the_job(job: ProjectJob) -> Union[str, Tuple]:
    """
    Different uprev would have different ways to classify jobs. So we would
    specialize this method depending on the pair to uprev.
    :param job: ProjectJob
    :return: key
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return __unshard_job_name(job.name)
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return __unshard_job_name(job.name), job.stage


def __download_artifacts(job: ProjectJob, artifacts: dict) -> bool:
    """
    For a given job, find if it has left artifacts (there could be more than
    one location to look at) to be downloaded. If so, collect it to be
    processed next. On the contrary, report returning false.
    :return:
    """

    def failures_path_generator() -> str:
        yield "results/failures.csv"
        yield f"results/{__unshard_job_name(job.name)}/failures.csv"

    failures_path = ""
    try:
        if job.name not in artifacts:
            artifact = None
            for failures_path in failures_path_generator():
                try:
                    artifact = job.artifact(failures_path)
                except gitlab.exceptions.GitlabGetError as inner_exception:
                    code = inner_exception.response_code
                    if code == 404:
                        # go to check the next path with the generator
                        continue
                    raise inner_exception
                debug(
                    f"Downloaded artifacts for '{job.name}' "
                    f"from '{failures_path}' at {job.web_url}"
                )
            if artifact is None:
                raise FileNotFoundError(f"No artifacts found")
            artifacts[job.name] = artifact
        return True
    except Exception as outer_exception:
        debug(
            f"An exception ({type(outer_exception)}) occurred "
            f"when downloading results for '{job.name}' at {job.web_url}: "
            f"{outer_exception}!"
        )
        return False


def __collate_failures(results: bytes, job_failures: dict, recollect: bool) -> None:
    """
    Process the lines in the results downloaded to insert the data to the
    failures structure of a given job.
    :param results: content of the artifact downloaded
    :param job_failures: key-value of the test and the results it has had
    :param recollect: if this happened after a failed jobs retry
    :return:
    """
    for line in results.decode("utf-8").split("\n"):
        if not line.strip():
            continue
        test_name, result = line.split(",")
        # detect flakiness: collect the current execution results
        job_failures[test_name].append(result)
        if recollect and len(job_failures[test_name]) == 1:
            # As we only collect when fail is reported or unexpected pass,
            #  complete the information
            failures_item = job_failures[test_name]
            if result == "UnexpectedPass":
                failures_item.appendleft("ExpectedFail")
            else:
                failures_item.appendleft("Pass")


def __implicit_results_become_explicit(failures: dict) -> None:
    """
    review and complete the list of pairs with the implicit pass
    :param failures: dictionary with the results already collected
    :return: dictionary with the implicit results made explicit
    """
    for test_dict in failures.values():
        for results_deque in test_dict.values():
            if len(results_deque) == 1:
                if results_deque[0] == "UnexpectedPass":
                    results_deque.append("ExpectedFail")
                else:
                    results_deque.append("Pass")


def __unshard_job_name(name: str) -> str:
    """
    Remove the suffix on the sharded jobs to have a single name for all of them.
    :param name: string
    :return: original name before sharding
    """
    search = re.search(r"(.*) [0-9]+/[0-9]+$", name)
    if search:  # if it has a " n/m" at the end, remove it.
        return search.group(1)
    return name


def update_branch(
    repo: git.Repo,
    failures: dict,
    do_commit: bool = True,
) -> None:
    """
    Using the local clone of the repository, review the failures found
    (if there are any) to update the expectations (if required).
    :param repo: working repository
    :param failures: dictionary of the failures to update
    :param do_commit: flag to define if the clone should be modified
    :return:
    """
    os.chdir(repo.working_dir)
    try:
        index = repo.index  # This is expensive, reuse the index object
        update_expectations = False
        for job_key, test_dict in failures.items():
            expectations = __get_expectations_path_and_file(job_key)
            if expectations is None:
                continue
            fails, flakes, skips = __get_expectations_contents(*expectations)
            existing_fails = __fail_tests_and_their_causes(fails["content"])

            fixed_tests, update_expectations = __include_new_fails_and_flakes(
                job_key,
                test_dict,
                flakes,
                fails,
                skips,
                existing_fails,
            )

            __remove_fixed_tests(fixed_tests, fails["content"])

            for file_name, content in [
                (flakes["name"], flakes["content"]),
                (fails["name"], fails["content"]),
                (skips["name"], skips["content"]),
            ]:
                __patch_content(index, file_name, content, do_commit)

        if do_commit:
            if update_expectations:
                repo.git.commit("--amend", "--no-edit")
                debug(f"Amended commit with new SHA " f"{repo.head.commit.hexsha}\n")
            __rename_active_branch(repo, Branch().for_merge_request)
    finally:
        os.chdir("..")


def __rename_active_branch(repo: git.Repo, new_name: str) -> None:
    """
    To complete the rename action, it is necessary to do it in two steps
    because the gitpython operation doesn't remove the old one in the remote.
    The new branch is only local now. The step to push it comes next by call
    push_to_merge_request().
    :param repo: working repository
    :param new_name: destination name for the temporary branch
    :return:
    """
    repo.active_branch.rename(new_name, force=True)
    try:
        # This branch is exceedingly rare in the remote, but it may happen if
        #  there is an interrupted execution of `ci-uprev` that pushes the
        #  branch and breaks before continuing the process.
        repo.remotes.fork.push(refspec=f":{Branch().for_wip}")
        debug(f"Remove the {Branch().for_wip} in the remote fork project.")
    except git.exc.GitCommandError:
        debug(f"The remote fork project doesn't have an orphan {Branch().for_wip}")


def __get_expectations_path_and_file(job_key: str) -> Optional[Tuple]:
    """
    Find in the structure where are located the expectation files for a given
    job. So it returns two strings. The first is with the path within the
    project. The second is with the prefix of the files that store the fails,
    flakes, and skips.
    :param job_key: string
    :return: a pair of strings
    """
    try:
        uprev_pair = UpdateRevisionPair().enum
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            expectations_paths = virglrenderer_expectations_paths
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            expectations_paths = mesa_expectations_paths
        else:
            target_project = Target().gl_project
            raise NotImplementedError(
                f"Not yet implemented the expectations "
                f"update for {target_project.name}"
            )
        path = expectations_paths[job_key]["path"]
        files = expectations_paths[job_key]["files"]
        if not path or not files:
            raise KeyError
        debug(f"For {job_key!r} the path is {path!r} and files {files!r}")
        return path, files
    except KeyError:
        debug(f"The tool is not yet managing expectations for {job_key}")
        return


def __get_expectations_contents(path: str, prefix: str) -> Tuple[dict, dict, dict]:
    """
    Get the contents of the files that could be necessary to update the
    expectations.
    :param path: string
    :param prefix: string
    :return: three dicts with the name and content of the files
    """
    fails_file_name = f"{path}/{prefix}-fails.txt"
    fails = {"name": fails_file_name, "content": __get_file_content(fails_file_name)}
    flakes_file_name = f"{path}/{prefix}-flakes.txt"
    flakes = {"name": flakes_file_name, "content": __get_file_content(flakes_file_name)}
    skips_file_name = f"{path}/{prefix}-skips.txt"
    skips = {"name": skips_file_name, "content": __get_file_content(skips_file_name)}
    return fails, flakes, skips


def __fail_tests_and_their_causes(fails_contents: list) -> dict:
    """
    From the list of lines in the fails file, convert it to a dictionary where
    the key is the test name, and the value is the test results and the line.
    We can later check if the test has failed for the same reason or another.
    :param fails_contents:
    :return:
    """
    dct = {}
    for i, line in enumerate(fails_contents, start=1):
        if line.startswith("#") or line.count(",") == 0:
            continue  # ignore these lines
        test_name, test_result = line.strip().rsplit(",", 1)
        dct[test_name] = {"result": test_result, "line": i}
    return dct


def __include_new_fails_and_flakes(
    job_key: str,
    tests: dict,
    flakes: dict,
    fails: dict,
    skips: dict,
    existing_fails: dict,
) -> Tuple[list, bool]:
    """
    Prepare the content for the fails and flake files with the newer
    information from the uprev pipeline execution.
    :rtype: object
    :param job_key: identifier
    :param tests: list
    :param flakes: file name and content
    :param fails: file name and content
    :param skips: file name and content
    :param existing_fails: dict with the known fails and their result
    :return: list of test that are fix with the uprev, flag about if it has
             made an expectations update
    """
    expectations_change = False
    fixed_tests = []
    for test_name, test_results in tests.items():
        # remove duplicated elements
        test_results_unique = set(test_results)
        assert (
            len(test_results_unique) > 0
        ), "Failed, tests should have at least one result"
        if len(test_results_unique) != 1:
            debug(
                f"Flaky test detected in {job_key} "
                f"{test_name=} with results {list(test_results)}"
            )
            flakes["content"] += [f"{test_name}\n"]
            expectations_change = True
        elif test_results[0] == "UnexpectedPass":
            debug(f"UnexpectedPass test detected in {job_key} " f"{test_name=}")
            fixed_tests += [test_name]
            expectations_change = True
        else:
            if test_results[0] in ["Timeout"]:
                file_name = skips["name"]
                content = skips["content"]
            else:
                file_name = fails["name"]
                content = fails["content"]
            if test_name in existing_fails.keys():
                previous_test_result = existing_fails[test_name]["result"]
                line = existing_fails[test_name]["line"]
                if test_results[0] == previous_test_result:
                    debug(
                        f"Failed test detected in {job_key} {test_name=} "
                        f"with results '{test_results[0]}' was already in "
                        f"{file_name}:{line} with the same cause."
                    )
                    # This shouldn't happen, but log it just in case.
                else:
                    debug(
                        f"Failed test detected in {job_key} {test_name=} "
                        f"with results '{test_results[0]}' was already in "
                        f"{file_name}:{line} with a different cause "
                        f"(was '{previous_test_result}')."
                    )
                    content[line - 1] = f"{test_name},{test_results[0]}\n"
                    expectations_change = True
            else:
                debug(
                    f"Failed test detected in {job_key} {test_name=} "
                    f"with results {test_results[0]}"
                )
                content += [f"{test_name},{test_results[0]}\n"]
                expectations_change = True
    return fixed_tests, expectations_change


def __remove_fixed_tests(fixed_tests: list, fails_content: list) -> None:
    """
    Review the list of tests that are fixed with the uprev to remove them from
    the list of tests that it was known they fail.
    :param fixed_tests:
    :param fails_content:
    :return:
    """
    for test_name in fixed_tests:
        found = False
        if f"{test_name},Fail\n" in fails_content:
            fails_content.remove(f"{test_name},Fail\n")
            found = True
        if f"{test_name},Crash\n" in fails_content:
            fails_content.remove(f"{test_name},Crash\n")
            found = True
        if found:
            debug(f"Fixed test {test_name} removed from the fails file")
        else:
            debug(f"Fixed test {test_name} is not present in the fails file")


def __patch_content(
    index: git.index.base.IndexFile, file_name: str, contents: list, add_index: bool
) -> None:
    """
    Once the file's content is ready, write in the file and add it to the git
    repo to prepare it to be committed.
    :param index: gitpython object to control the commit
    :param file_name: string
    :param contents: list of lines
    :param add_index: flag to include the change to a near commit
    :return:
    """
    with open(file_name, "wt") as file_descriptor:
        file_descriptor.writelines(contents)
    debug(f"Patched file {file_name}")
    if add_index:
        index.add(file_name)


def __get_file_content(file_name: str) -> list:
    """
    Return the lines in a file as a list.
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        with open(file_name, "rt") as file_descriptor:
            return file_descriptor.readlines()
    else:
        return list()


def push_to_merge_request(
    repo: git.Repo, failures: dict, pipeline: ProjectPipeline
) -> None:
    """
    Once we have a viable candidate to uprev, generate or append to an existing
    merge request the proposal. It can be with or without an update on the
    expectations. Once the merge request has a pipeline to verify this proposal,
    the tool triggers it.
    :param repo: git.Repo
    :param failures: dict
    :param pipeline: ProjectPipeline
    :return: None
    """
    push(repo)
    regressions, flakes, unexpectedpass = __get_failures_by_category(failures)
    note_comments = __prepare_comments(pipeline, regressions, flakes, unexpectedpass)
    merge_request = __publish_uprev_merge_request()
    __append_note_to_merge_request(merge_request, note_comments)
    __run_merge_request_pipeline(merge_request, repo.head.commit.hexsha)


def __get_failures_by_category(failures: dict) -> Tuple[dict, dict, dict]:
    """
    From the collection of failures, classify them by category if there are
    regressions or flakes.
    :param failures: dict
    :return: Tuple[regressions, flakes, unexpectedpass]
    """
    regressions_per_category = defaultdict(dict)
    flake_per_category = defaultdict(dict)
    unexpectedpass_per_category = defaultdict(dict)
    for job_name, test_dict in failures.items():
        expectations = __get_expectations_path_and_file(job_name)
        if expectations is None:
            alert = " (Missing expectations update in the proposal)."
        else:
            alert = ""
        for test_name, results in test_dict.items():
            results_unique = set(results)
            if len(results_unique) != 1:
                category = f"Unreliable tests on {job_name}{alert}:\n"
                flake_per_category[category][test_name] = list(results)
            elif results[0] != "UnexpectedPass":
                category = f"Possible regressions on {job_name}{alert}:\n"
                regressions_per_category[category][test_name] = results[0]
            else:
                assert len(results_unique) == 1
                assert results[0] == "UnexpectedPass"
                category = f"Fix test with the uprev on {job_name}{alert}:\n"
                unexpectedpass_per_category[category][test_name] = results[0]
    return (regressions_per_category, flake_per_category, unexpectedpass_per_category)


def __prepare_comments(
    pipeline: ProjectPipeline,
    regressions_per_category: dict,
    flake_per_category: dict,
    unexpectedpass_per_category: dict,
) -> Tuple[list, list, list]:
    """
    With the information available, prepare the list of comments to be added
    in a note in the merge request.
    :param pipeline: ProjectPipeline
    :param regressions_per_category: dict
    :param flake_per_category: dict
    :param unexpectedpass_per_category: dict
    :return: list
    """
    comments = (
        __source_comment(pipeline),
        __generate_comment(regressions_per_category, False),
        __generate_comment(flake_per_category, True),
        __generate_comment(unexpectedpass_per_category, False),
    )
    return comments


def __publish_uprev_merge_request() -> ProjectMergeRequest:
    """
    Check if a merge request is already in progress or create a newer one.
    :return: ProjectMergeRequest
    """
    fork_project = Fork().gl_project
    uprev_branch = Branch().for_merge_request
    target_project = Target().gl_project
    mr_title = Title().for_merge_request
    merge_request = __get_uprev_merge_request(mr_title)

    if merge_request is None:
        labels = __get_uprev_labels()
        merge_request = fork_project.mergerequests.create(
            {
                "source_branch": uprev_branch,
                "target_branch": target_project.default_branch,
                "target_project_id": target_project.id,
                "title": mr_title,
                "labels": labels,
            }
        )
        debug(f"Created merge request {merge_request.web_url}")
    else:
        debug(f"Pushed to existing merge request {merge_request.web_url}")
    __link_merge_request_with_issue(merge_request)
    return merge_request


@cache
@retry(stop=stop_after_attempt(10), wait=wait_exponential(multiplier=1, max=180))
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_uprev_merge_request(title: str) -> Optional[ProjectMergeRequest]:
    username = GitlabProxy().gl_obj.user.username
    uprev_branch = Branch().for_merge_request
    target_project = Target().gl_project
    query = {
        "state": "opened",
        "author_username": username,
        "search": title,
        "source_branch": uprev_branch,
    }
    try:
        mr = next(
            target_project.mergerequests.list(query_parameters=query, iterator=True)
        )
        return target_project.mergerequests.get(mr.iid)
    except StopIteration:
        # to scape tenacity when the exception is because there isn't such MR.
        return None


def __link_merge_request_with_issue(merge_request: ProjectMergeRequest) -> None:
    issue = __get_uprev_issue()
    if isinstance(issue, ProjectIssue):
        description = [
            f"Uprev candidate generated in merge request "
            f"{merge_request.web_url}. The issue is solved."
        ]
        issue.notes.create({"body": "".join(description)})
        if in_production():
            try:
                issue.state_event = "close"
                issue.save()
            except gitlab.exceptions.GitlabUpdateError as exception:
                debug(
                    f"Exception '{exception.error_message}' when closing the "
                    f"issue about previous problems to uprev. "
                    f"See {issue.web_url}"
                )
    else:
        debug(
            f"There isn't a previously in progress issue about this uprev to "
            f"report this new uprev candidate."
        )


def __append_note_to_merge_request(
    merge_request: ProjectMergeRequest, comments: Tuple[list, list, list]
) -> None:
    """
    With new or in-progress merge requests, add a note with information about
    the current proposal to uprev.
    :param merge_request: ProjectMergeRequest
    :param comments: list
    :return:
    """
    comments = ["".join(c) for c in comments if c]
    merge_request.notes.create({"body": "\n".join(comments)})


def __run_merge_request_pipeline(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> None:
    """
    Once the merge request has the information about the uprev proposal, a
    pipeline is created to verify that the proposal doesn't brake previous work.
    Creating, triggering, and monitoring the evolution is the last part of the
    tasks of this tool.
    :param mergerequest: ProjectMergeRequest
    :return: None
    """
    merge_request = __wait_until_merge_request_commit_matches(
        merge_request, commit_hash
    )
    # Depending on the groups the user belongs, this merge request could have the pipeline in the fork or
    #  in the main project. So, we must check where the information we have points to continue the process.
    merge_request = __wait_until_merge_request_head_pipeline_commit_hash_matches(
        merge_request, commit_hash
    )
    merge_request = __check_merge_request_source(merge_request)
    __check_merge_request_pipeline_project(merge_request)
    merge_request_pipeline = __wait_pipeline_creation(
        commit_hash, source="merge_request_event"
    )
    failures = run(merge_request_pipeline)
    if failures:
        msg = (
            f"Something wrong in the final pipeline "
            f"{merge_request_pipeline.web_url}"
        )
        debug(f"{msg}\n{pformat(failures)}")
        raise RuntimeError(msg)


def __wait_until_merge_request_commit_matches(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> ProjectMergeRequest:
    """
    This method will wait until the merge request points to the expected commit hash
    :param merge_request: merge request where the commit was pushed
    :param commit_hash: sha1 of the commit previously pushed
    :return: refreshed merge request object
    """
    first_attempt = True
    while True:
        if merge_request.sha == commit_hash:
            debug(
                f"Merge request with latest commit {merge_request.sha}. "
                f"Look for its merge request pipeline!"
            )
            return merge_request
        if first_attempt:
            debug(f"Merge request doesn't have yet the latest commit. Wait...")
            first_attempt = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __wait_until_merge_request_head_pipeline_commit_hash_matches(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> ProjectMergeRequest:
    """
    This method will wait until the head pipeline of the merge request points to the expected commit hash
    :param merge_request: merge request where the commit was pushed
    :param commit_hash: sha1 of the commit previously pushed
    :return: refreshed merge request object
    """
    first_attempt_of_none = True
    first_attempt_of_sha = True
    while True:
        if merge_request.head_pipeline is not None:
            if merge_request.head_pipeline["sha"] == commit_hash:
                debug(
                    f"Merge request head pipeline {merge_request.head_pipeline['id']} "
                    f"points to the latest commit {merge_request.head_pipeline['sha']}."
                )
                return merge_request
            if first_attempt_of_sha:
                debug(
                    f"Merge request head pipeline {merge_request.head_pipeline['id']} "
                    f"doesn't point yet to the latest commit. Wait..."
                )
                first_attempt_of_sha = False
        elif first_attempt_of_none:
            debug(f"Merge request doesn't have yet a head pipeline. Wait...")
            first_attempt_of_none = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __check_merge_request_source(
    merge_request: ProjectMergeRequest,
) -> ProjectMergeRequest:
    """
    This method will wait until the head pipeline of the merge request corresponds to one from the merge request event.
    :param merge_request: merge request where the uprev was pushed
    :return: refreshed merge request object
    """
    first_attempt = True
    while True:
        if merge_request.head_pipeline["source"] == "merge_request_event":
            debug(
                f"Head pipeline {merge_request.head_pipeline['id']} is from "
                f"a merge request event ({merge_request.head_pipeline['source']})."
            )
            return merge_request
        if first_attempt:
            debug(
                f"Head pipeline {merge_request.head_pipeline['id']} doesn't "
                f"point yet to merge request event. Wait..."
            )
            first_attempt = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __check_merge_request_pipeline_project(merge_request: ProjectMergeRequest) -> None:
    """
    Depending on the rights of the user running the uprev, the merge request pipeline is created in the main project
    or in the fork. So, this method checks in which of them is created and configures the tool to proceed accordingly.
    :param merge_request: merge request where the uprev was pushed
    :return:
    """
    project_id = merge_request.head_pipeline["project_id"]
    project = GitlabProxy().gl_obj.projects.get(project_id)
    debug(
        f"Merge request with the pipeline {merge_request.head_pipeline['id']} "
        f"from the latest commit (in {project.path_with_namespace}). "
        f"Run this pipeline! {merge_request.head_pipeline['web_url']}"
    )
    if Target().gl_project == project:
        Working().project = Target()
    elif Fork().gl_project == project:
        Working().project = Fork()
    else:
        raise RuntimeError(
            f"The merge request pipeline is in project {project.path_with_namespace}, not the Target or the Fork."
        )


@retry(stop=stop_after_attempt(10), wait=wait_exponential(multiplier=1, max=180))
def __get_merge_request(mergerequest_id: int) -> ProjectMergeRequest:
    """
    Get a merge request object from the target project. If there is an
    exception, use tenacity to retry.
    A practical use of this method is to refresh the gitlab object.
    :param mergerequest_id: merge request identifier
    :return:
    """
    return Target().gl_project.mergerequests.get(mergerequest_id)


def __source_comment(pipeline: ProjectPipeline) -> list:
    """
    With environment information, prepare a few lines for the merge request
    note reporting how this proposal has been produced.
    :param pipeline: ProjectPipeline
    :return: lines as list
    """
    job_id = os.getenv("CI_JOB_ID")
    if job_id is not None:
        gl = GitlabProxy().gl_obj
        uprev_project = gl.projects.get(os.getenv("CI_PROJECT_ID"))
        uprev_job = uprev_project.jobs.get(job_id)
        source_comment = [
            f"This information comes from the execution of ci-uprev in job "
            f"[{uprev_job.id}]({uprev_job.web_url}).\n"
        ]
    else:
        source_comment = [
            f"This information comes from a local execution of ci-uprev.\n"
        ]
    pipelines_summary = f"Related pipeline: " f"[{pipeline.id}]({pipeline.web_url})"
    pipelines_summary += ".\n"
    source_comment += [pipelines_summary]
    dependency_diff = Dependency().diff
    if dependency_diff:
        source_comment.append(
            f"The dependency changes can be reviewed with "
            f"this [compare]({dependency_diff}).\n"
        )
    return source_comment


def __generate_comment(by_category: dict, as_list: bool) -> list:
    """
    Transform the dictionary with the information by category to the format we
    place this report in the merge request note.
    :param by_category: dict
    :param as_list: bool
    :return: lines as list
    """
    comment = []
    for category, problem in by_category.items():
        category_comment = [category]
        for test_name, result in problem.items():
            if as_list:
                line = f"- {test_name}, {list(result)}\n"
            else:
                line = f"- {test_name}, {result}\n"
            category_comment.append(line)
        category_comment = __truncate_long_messages(category_comment)
        category_comment.append("\n")
        comment.extend(category_comment)
    return comment


def __truncate_long_messages(messages: list[str]) -> list[str]:
    """
    When a message list of lines is too long and could disturb the reading of
    the user, this method packs the lines over a limit in a details Markdown
    code.
    :param messages: lines in a message
    :return: original or, if needed, truncated message
    """
    if len(messages) > LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:
        messages, details, excedent = (
            messages[:LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE],
            messages[
                LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:MAX_LINES_PER_CATEGORY_IN_NOTE
            ],
            messages[MAX_LINES_PER_CATEGORY_IN_NOTE:],
        )
        messages.append("\n\n<details>\n")
        messages.append("\n<summary>... and more</summary>\n\n")
        messages.extend(details)
        if excedent:
            # TODO: link to the commit content
            messages.append("\nToo much, to see more check the file in the commit\n")
        messages.append("\n</details>\n")
    return messages


def main():
    args = __cli_arguments()

    if args.subcommand == "collate":
        __cmd_collate_results(args.PIPELINE_ID, args.patch, args.branch, args.diff_file)
    elif args.subcommand == "retry":
        __cmd_retry_pipeline(args.PIPELINE_ID)
    elif args.subcommand == "continue":
        __cmd_continue_uprev(args.PIPELINE_ID)
    else:
        try:
            __cmd_uprev(args.revision, args.pipeline)
        except NothingToUprev:
            debug(
                f"Latest commit in the dependency project is the revision "
                f"already in use in the target project. No need for uprev."
            )
            return


def __cli_arguments() -> Namespace:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to update the revision of a project used "
        "inside another project."
    )
    # TODO: mention that no argument is required
    #  all the configuration elements come from environment variables and this
    #  arguments are only for development and debugging.
    #  So, also show help about those envvars
    #   TARGET_PROJECT_PATH, FORK_PROJECT_PATH, DEP_PROJECT_PATH, GITLAB_TOKEN
    parser.add_argument(
        "--revision", metavar="HASH", help="Specify the revision to uprev."
    )
    parser.add_argument(
        "--pipeline",
        metavar="PIPELINE_ID",
        help="Specify the pipeline to reference in the uprev. "
        "Requires to have specified the revision.",
    )
    subparsers = parser.add_subparsers(
        title="subcommands",
        dest="subcommand",
        description="valid subcommands",
        help="sub-command additional help",
    )
    parser_collateresults = subparsers.add_parser(
        "collate", help="Collate results from an existing pipeline."
    )
    parser_collateresults.add_argument(
        "--patch",
        action="store_true",
        help="After collate results, prepare a " "patch to update expectations.",
    )
    parser_collateresults.add_argument(
        "--branch",
        metavar="BRANCH",
        help="Specify the branch of the " "repository that will be patched.",
    )
    parser_collateresults.add_argument(
        "--diff-file",
        metavar="NAME",
        help="Specify the name of the output " "patch file.",
    )
    parser_collateresults.add_argument(
        "PIPELINE_ID", help="Only collate results of the pipeline."
    )
    parser_retrypipeline = subparsers.add_parser(
        "retry", help="Retry an existing pipeline."
    )
    parser_retrypipeline.add_argument(
        "PIPELINE_ID",
        help="Collate results and then retry failed jobs " "and collate again.",
    )
    parser_continue = subparsers.add_parser(
        "continue", help="Specify a point in the process to continue from"
    )
    parser_continue.add_argument(
        "PIPELINE_ID",
        help="Continue like if the execution was working with " "this pipeline",
    )
    return parser.parse_args()


def __cmd_collate_results(
    pipeline_id: int,
    create_patch: bool,
    branch: str,
    patch_name: str,
) -> None:
    """
    Get the failures and report about them
    :param pipeline_id: identification number
    :param create_patch: decide if the output is a summary or a patch to the
                         expectations on the target project.
    :param branch: Specify the branch to be patched
    :param patch_name: Specify the file name of the diff file generated
    :return:
    """
    Working().project = Fork()
    working_project = Working().gl_project
    pipeline = working_project.pipelines.get(pipeline_id)
    msg = (
        f"Only collate results from pipeline {pipeline_id} "
        f"in {working_project.path_with_namespace}"
    )
    if create_patch:
        msg = f"{msg} and provide a patch to update expectations"
    debug(f"{msg}.")
    failures, _, jobs_without_artifacts = collate_results(pipeline)
    if jobs_without_artifacts:
        job_names = [job.name for job in jobs_without_artifacts]
        debug(f"These jobs failed to produce artifacts: " f"{', '.join(job_names)}")
    if failures:
        copy_failures = defaultdict(defaultdict)
        for job in failures:
            for test in failures[job]:
                # remove the deque
                copy_failures[str(job)][test] = list(failures[job][test])
                # TODO: the pipeline may have retries.
                # By now, assume they are not flakes so duplicate the failures
                # lists to patch them on fails.
                failures[job][test].append(failures[job][test][0])
        # remove the defaultdict
        copy_failures = json.loads(json.dumps(copy_failures))
        debug(f"Failures:\n{pformat(copy_failures)}")
        if create_patch:
            repo = __clone_repo(force_fresh_repo=True)
            if branch:
                target_project = Target().gl_project
                repo.heads[target_project.default_branch].checkout(force=True, b=branch)
            update_branch(repo, failures, do_commit=False)
            patch_name = (
                patch_name if patch_name else f"{pipeline_id}_expectations_update.diff"
            )
            with open(patch_name, "wt") as file_descriptor:
                file_descriptor.writelines(repo.git.diff())


def __cmd_retry_pipeline(pipeline_id: int) -> None:
    """
    Direct access to the pipeline post-process. When there is a problem with
    how a pipeline has finished and/or how the ci-uprev did to post-process,
    this is the option to review under similar conditions.
    :param pipeline_id: identification number
    :return:
    """
    Working().project = Fork()
    working_project = Working().gl_project
    pipeline = working_project.pipelines.get(pipeline_id)
    debug(
        f"Collate current results of the pipeline {pipeline_id} and retry"
        f"in {working_project.path_with_namespace}."
    )
    failures = __post_process_pipeline(pipeline)
    if failures:
        debug(f"Failures:\n{pformat(failures)}")


def __cmd_continue_uprev(pipeline_id: int) -> None:
    """
    This is a debug purpose option. When an uprev didn't finish as expected,
    instead of fix and relaunch from scratch, this option can cling to the
    pipeline of a previous uprev and proceed like if it did it from the
    beginning.
    :param pipeline_id:
    :return:
    """
    repo = git.Repo(LocalClone().directory)
    Working().project = Fork()
    working_project = Working().gl_project
    pipeline = working_project.pipelines.get(pipeline_id)
    failures = __post_process_pipeline(pipeline)
    update_branch(repo, failures)
    push_to_merge_request(repo, failures, pipeline)


def __cmd_uprev(
    revision: str,
    pipeline_id: int,
) -> None:
    """
    This is the default procedure of the uprev tool. Without options, the
    default, it will search for a pipeline in the project to uprev that
    satisfies the necessary conditions. The commit of this pipeline will be used
    to uprev in the target project. (Those two variables, revision and pipeline,
    can be fixed using option arguments for debug purposes like repeat and
    compare with a previous execution.)

    Then the uprev candidate is tested, if there are expectations to update
    they are and the commit amended, to then generate a merge request proposal.

    :param revision: optional parameter to specify the revision to use
    :param pipeline_id: optional parameter to specify the pipeline to use
    :return:
    """
    dep_project = Dependency().gl_project
    if not revision and not pipeline_id:
        pipeline_id, revision = get_candidate_for_uprev()
        debug(
            f"Found pipeline {pipeline_id} with SHA {revision} "
            f"{dep_project.commits.get(revision).web_url}"
        )
    else:
        if revision:
            pipeline_id = __search_pipeline(revision)
            debug(
                f"Specified the revision to uprev to {revision} and found "
                f"the pipeline {pipeline_id} "
                f"{dep_project.commits.get(revision).web_url}"
            )
        elif pipeline_id:
            revision = __get_pipeline_commit(pipeline_id)
            debug(
                f"Specified the pipeline to uprev to {pipeline_id} and found "
                f"the revision {revision} "
                f"{dep_project.commits.get(revision).web_url}"
            )
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        templates_commit = get_templates_commit(revision)
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        templates_commit = get_templates_commit("main")
    else:
        raise NotImplementedError(
            f"Not yet implemented the uprev of " f"{uprev_pair.name}"
        )
    repo = create_branch(pipeline_id, revision, templates_commit)
    pipeline = push(repo)
    failures = run(pipeline)
    update_branch(repo, failures)
    push_to_merge_request(repo, failures, pipeline)


def __search_pipeline(revision: str) -> int:
    """
    When a revision is specified, the pipeline it has generated is needed.
    :param revision: dep project revision
    :return: pipeline id
    """
    dep_project = Dependency().gl_project
    pipelines = dep_project.pipelines.list(sha=revision, state="success")
    if not pipelines:
        raise ValueError(f"No pipeline found with this revision")
    return pipelines[-1].id


def __get_pipeline_commit(pipeline_id: int) -> str:
    """
    When a pipeline is specified, the revision that generated it is needed.
    :param pipeline_id: identification number
    :return: string with the commit sha
    """
    dep_project = Dependency().gl_project
    pipeline = dep_project.pipelines.get(pipeline_id)
    return pipeline.sha


if __name__ == "__main__":
    main()
