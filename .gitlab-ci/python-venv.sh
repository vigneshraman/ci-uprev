#!/bin/bash

set -e

echo -e "\e[0Ksection_start:`date +%s`:python3[collapsed=true]\r\e[0KInstall python3, the corresponding pip and git dependencies"
export `cat /etc/os-release | grep ^ID=`
if [ $ID == 'alpine' ]; then
    apk add --update --no-cache python3 py3-pip git build-base python3-dev
elif [ $ID == 'fedora' ]; then
    dnf install -y python3 python3-pip git gcc python3-devel
fi
# (build-base|gcc) is for ruamel.yaml
# and its dependency to have "Python.h" means (python3-{dev,devel}) install also
echo -e "\e[0Ksection_end:`date +%s`:python3\r\e[0K"

echo -e "\e[0Ksection_start:`date +%s`:virtualenv[collapsed=true]\r\e[0KSetup virtual environment"
pip3 install virtualenv
python3 -m virtualenv ci-uprev.venv
source ci-uprev.venv/bin/activate
pip install -r requirements.txt
echo -e "\e[0Ksection_end:`date +%s`:virtualenv\r\e[0K"

echo -e "\e[0Ksection_start:`date +%s`:ci-uprev[collapsed=true]\r\e[Install ci-uprev package"
pip install .
echo -e "\e[0Ksection_end:`date +%s`:ci-uprev\r\e[0K"
