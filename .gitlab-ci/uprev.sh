#!/bin/bash

set -e

echo -e "\e[0Ksection_start:`date +%s`:launch\r\e[0Kci-uprev"
ci-uprev
echo -e "\e[0Ksection_end:`date +%s`:launch\r\e[0K"
