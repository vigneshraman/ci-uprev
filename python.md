# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 22.3.1 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv ci-uprev.venv
$ source ci-uprev.venv/bin/activate
(...)
$ python -m pip install python-gitlab gitpython ruamel.yaml tenacity
$ pip list
Package            Version
------------------ --------
certifi            2023.5.7
charset-normalizer 3.1.0
gitdb              4.0.10
GitPython          3.1.31
idna               3.4
pip                23.1.2
python-gitlab      3.14.0
requests           2.30.0
requests-toolbelt  1.0.0
ruamel.yaml        0.17.26
ruamel.yaml.clib   0.2.7
setuptools         67.7.2
smmap              5.0.0
tenacity           8.2.2
urllib3            2.0.2
wheel              0.40.0
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### Development install

To install this module in the virtual environment in a way that changes do not 
require to reinstall it:

```bash
pip install -e .
```

Then one will have available the command:

```bash
ci-uprev
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze --all > requirements.txt
```

## Test environment

```bash
python3 -m virtualenv pytest.venv
source pytest.venv/bin/activate
pip install -r requirements.txt
python -m pip install pytest
```

The `python-gitlab-mock` dependency could be installed from the repo

```
pip install git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock.git
```

or from a local clone:

```
pip install -e ../python-gitlab-mock.git/
```

Finally it is time to install `ci-uprev` itself in the environment:

```bash
pip install -e .
```

Now, one can call `pytest`.
